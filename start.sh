#!/bin/bash

ARGS=("$@")
SESSION=${PWD##*/} # имя сесси tmux = имя директории

if ! [[ $(tmux ls | grep $SESSION) ]]; then
	tmux new-session -d -s $SESSION

	tmux split-window -h

	tmux select-pane -t 0
	tmux send-keys 'conda activate ' $SESSION C-m
	tmux send-keys 'python main.py' C-m

	tmux select-pane -t 1
	tmux send-keys 'conda activate ' $SESSION C-m
	tmux send-keys 'python processor.py' C-m
fi

# если выполнить с аргументом -d, то подключение к сессии не произойдет
if ! [[ $ARGS = "-d" || $ARGS = "--daemon" ]]; then
	tmux attach-session -t $SESSION
fi