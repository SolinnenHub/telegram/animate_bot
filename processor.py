import gc, os, time, shutil
from uuid import uuid4
import pymongo, bson, gridfs
from tools.processing import process
import config as cfg
import resources as R

mongo = pymongo.MongoClient(cfg.mongo)
db = mongo[cfg.db_name]
fs = gridfs.GridFS(db)
queue = db['queue']

try:
	shutil.rmtree('cache_processor')
except FileNotFoundError:
	pass
os.makedirs('cache_processor')

while True:
	try:
		items = []
		for item in queue.find({'done': False}):
			items.append(item)
	except Exception as e:
		print(f"{R.Time()} WARNING: {e}")
		time.sleep(2)
		continue

	for item in items:
		image_path = f'cache_processor/{uuid4()}.jpg'
		with open(image_path, 'wb') as f:
			f.write(item['image'])

		try:
			video_path = process(
				image_path=image_path,
				image_uuid=item['_id'],
				effect_name=item['effect'],
				duration=item['duration']
			)
		except Exception as e:
			os.remove(image_path)
			item['image'] = None
			item['done'] = True
			item['error'] = True
			queue.replace_one({'_id': item['_id']}, item)
			print(f"{R.Time()} {item['_id']} from {item['name']} ended up with error [{e}].")
			continue

		os.remove(image_path)

		item['done'] = True
		item['image'] = None
		fs.put(open(video_path, 'rb'), _id=item['_id'])
		queue.replace_one({'_id': item['_id']}, item)

		os.remove(video_path)
		print(f"{R.Time()} {item['_id']} from {item['name']} had been processed.")

		gc.collect()

	time.sleep(2)