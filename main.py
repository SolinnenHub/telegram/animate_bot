import os, asyncio, logging, time, shutil, random
import hashlib
import threading
import pymongo, bson, gridfs
from uuid import uuid4
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from aiogram.types import ParseMode, ChatActions
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.types.message import ContentType
from aiogram.contrib.middlewares.logging import LoggingMiddleware
from aiogram.utils.exceptions import (
	MessageNotModified, WrongFileIdentifier, TypeOfFileMismatch, RetryAfter, BotBlocked, BadRequest, RestartingTelegram)
from tools.mongo import updateObject, processChat
import resources as R
import config as cfg

logging.basicConfig(level=logging.WARNING)

bot = Bot(token=cfg.botToken)
dp = Dispatcher(bot)
dp.middleware.setup(LoggingMiddleware())

mongo = pymongo.MongoClient(cfg.mongo)
db = mongo[cfg.db_name]
fs = gridfs.GridFS(db)
chats = db['chats']
queue = db['queue']

mutex_db = asyncio.Lock()

async def showWelcome(chat, original):
	limit = 5

	size_free = 0
	for effect in R.Effects:
		if R.Effects[effect]['free']:
			size_free += 1

	row_free = []
	rows_free = []
	count_free = 0
	markup = InlineKeyboardMarkup()
	for effect in R.Effects:
		btn = InlineKeyboardButton(R.Effects[effect]['label'], callback_data=f'effect.{effect}')
		if R.Effects[effect]['free']:
			row_free.append(btn)
			count_free += 1
			if count_free % limit == 0 or count_free == size_free:
				rows_free.append(row_free)
				row_free = []

	for row in rows_free:
		markup.row(*row)

	await bot.send_message(chat['_id'], R.Strings.Messages.Welcome(chat['lang']), reply_markup=markup, parse_mode=ParseMode.HTML)

async def showLanguageSelection(chat, original):
	markup = InlineKeyboardMarkup()
	en = InlineKeyboardButton(R.Strings.Buttons.Languages(R.Langs.EN), callback_data=f'langSelected.{R.Langs.EN}')
	ru = InlineKeyboardButton(R.Strings.Buttons.Languages(R.Langs.RU), callback_data=f'langSelected.{R.Langs.RU}')
	markup.row(en, ru)
	if original:
		try:
			await original.edit_text(R.Strings.Messages.Language(chat['lang']), reply_markup=markup)
		except MessageNotModified:
			pass
	else:
		await bot.send_message(chat['_id'], R.Strings.Messages.Language(chat['lang']), reply_markup=markup)

@dp.callback_query_handler(lambda c: 'langSelected' in c.data)
async def callback(query: types.CallbackQuery):
	chat = await processChat(mutex_db, chats, query.message.chat.id)
	chat['lang'] = query.data.split('.')[-1]
	await updateObject(mutex_db, chats, chat)
	if chat['newbie']:
		await query.message.delete()
		await showWelcome(chat, original=None)
	else:
		await showLanguageSelection(chat, original=query.message)
	chat['newbie'] = False
	await updateObject(mutex_db, chats, chat)
	await query.answer()

@dp.callback_query_handler(lambda c: 'effect' in c.data)
async def callback(query: types.CallbackQuery):
	chat = await processChat(mutex_db, chats, query.message.chat.id)
	effect = query.data.split('.')[-1]
	chat['effect'] = effect
	if not R.Effects.get(effect, None):
		chat['effect'] = next(iter(R.Effects))
	await updateObject(mutex_db, chats, chat)
	await query.answer(R.Effects[effect]['label'])

@dp.message_handler(commands=['start'])
async def handler(message: types.Message):
	chat = await processChat(mutex_db, chats, message.chat.id)
	if chat['newbie']:
		await showLanguageSelection(chat, original=None)
	else:
		await showWelcome(chat, original=None)

@dp.message_handler(commands=['language'])
async def handler(message: types.Message):
	chat = await processChat(mutex_db, chats, message.chat.id)
	await showLanguageSelection(chat, original=None)

def getMention(message):
	if message['from'].username:
		return '@'+message["from"].username
	else:
		return message.from_user.get_mention()

def md5sum(path):
	hash_md5 = hashlib.md5()
	with open(path, 'rb') as f:
		for chunk in iter(lambda: f.read(4096), b""):
			hash_md5.update(chunk)
	return hash_md5.hexdigest()

async def process(chat, message, file_id, reply=True):
	image_uuid = str(uuid4())
	image_path = f'cache/{image_uuid}.jpg'
	await bot.download_file_by_id(file_id, image_path)

	count = queue.count_documents(({'timestamp': {"$lt": time.time()}}))

	md5 = md5sum(image_path)
	if queue.count_documents({'chat_id': chat['_id'], 'md5': md5, 'effect': chat['effect']}):
		os.remove(image_path)
		await message.reply(R.Strings.Messages.AlreadyProcessing(chat['lang'], count), reply=reply)
		print(f"{R.Time()} {image_uuid} from {getMention(message)} discarded.")
		return

	with open(image_path, 'rb') as f:
		queue.insert_one({
			'_id': image_uuid,
			'chat_id': chat['_id'],
			'name': getMention(message),
			'done': False,
			'error': False,
			'md5': md5,
			'timestamp': int(time.time()),
			'image': bson.binary.Binary(f.read()),
			'effect': chat['effect'],
			'duration': 10
		})
	os.remove(image_path)

	await message.reply(R.Strings.Messages.Wait(chat['lang'], count), reply=reply)
	print(f"{R.Time()} {image_uuid} from {getMention(message)} approved.")

@dp.message_handler(commands=['animate'])
async def handler(message: types.Message):
	chat = await processChat(mutex_db, chats, message.chat.id)
	profile_photos = await bot.get_user_profile_photos(chat['_id'], offset=0, limit=1)
	if profile_photos.photos:
		file_id = profile_photos.photos[-1][-1].file_id
		await process(chat, message, file_id, reply=False)
	else:
		await bot.send_message(chat['_id'], R.Strings.Messages.ErrorNoProfilePicture(chat['lang']))

@dp.message_handler(content_types=ContentType.PHOTO)
async def handler(message: types.Message):
	if message.chat.id != cfg.adminChatId:
		await message.forward(cfg.adminChatId, disable_notification=True)
	chat = await processChat(mutex_db, chats, message.chat.id)
	file_id = message.photo[-1].file_id
	await process(chat, message, file_id)

@dp.message_handler(content_types=ContentType.ANIMATION)
async def handler(message: types.Message):
	if message.chat.id == cfg.adminChatId:
		file_id = message.animation.file_id
		return await message.reply(f'`{file_id}`', parse_mode=ParseMode.MARKDOWN)

@dp.errors_handler(exception=RestartingTelegram)
async def restarting_telegram(update: types.Update, exception: Exception):
	pass

async def run():
	try:
		shutil.rmtree('cache')
	except FileNotFoundError:
		pass
	os.makedirs('cache')

	while True:
		try:
			for item in queue.find({'done': True}):
				if item['error']:
					chat = chats.find_one({'_id': item['chat_id']})
					try:
						await bot.send_message(item['chat_id'], R.Strings.Messages.ErrorProcessing(chat['lang']))
					except BotBlocked:
						pass
					queue.delete_one({'_id': item['_id']})
					print(f"{R.Time()} {item['_id']} from {item['name']} ended up with error.")
					continue

				video_path = f'cache/{uuid4()}.mp4'
				with open(video_path, 'wb') as f:
					file = fs.find_one({'_id': item['_id']})
					if file:
						f.write(file.read())
					else:
						chat = chats.find_one({'_id': item['chat_id']})
						try:
							await bot.send_message(item['chat_id'], R.Strings.Messages.ErrorProcessing(chat['lang']))
						except BotBlocked:
							pass
						queue.delete_one({'_id': item['_id']})
						print(f"{R.Time()} {item['_id']} from {item['name']} ended up with error: file not found in Mongo.")
						continue

				try:
					await bot.send_chat_action(item['chat_id'], ChatActions.UPLOAD_VIDEO)
					await bot.send_animation(item['chat_id'], open(video_path, 'rb'))
				except BotBlocked:
					pass

				print(f"{R.Time()} {item['_id']} from {item['name']} done.")

				os.remove(video_path)
				fs.delete(item['_id'])
				queue.delete_one({'_id': item['_id']})

			await asyncio.sleep(2)
		except Exception as e:
			print(f"ERROR: {e}")
			await asyncio.sleep(2)

def repeat(coro, loop):
	asyncio.ensure_future(coro(), loop=loop)

if __name__ == '__main__':
	loop = asyncio.get_event_loop()
	loop.call_later(0, repeat, run, loop)
	executor.start_polling(dp, loop=loop, skip_updates=True)
