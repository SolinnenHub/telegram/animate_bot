import time
from datetime import datetime
from tools.blend import BlendModes

class Langs:
	RU = 'ru'
	EN = 'en'

Effects = {
	'flame': {
		'label': '🔥',
		'blend_mode': BlendModes.Screen,
		'alpha': 0,
		'demo': '', # animation file_id here
		'free': True
	},
	'flare': {
		'label': '✨',
		'blend_mode': BlendModes.Screen,
		'alpha': 0,
		'demo': '',
		'free': True
	},
	'neon_lines': {
		'label': '🟣',
		'blend_mode': BlendModes.Screen,
		'alpha': 0.32,
		'demo': '',
		'free': True
	},
	'comets': {
		'label': '💜',
		'blend_mode': BlendModes.Screen,
		'alpha': 0.3,
		'demo': '',
		'free': True
	},
	'green_water': {
		'label': '🥗',
		'blend_mode': BlendModes.Multiply,
		'alpha': 0,
		'demo': '',
		'free': True
	},
	'darkness': {
		'label': '🌘',
		'blend_mode': BlendModes.Multiply,
		'alpha': 0,
		'demo': '',
		'free': True
	},
	'darkness2': {
		'label': '🌑',
		'blend_mode': BlendModes.Multiply,
		'alpha': 0.05,
		'demo': '',
		'free': True
	},
	'code': {
		'label': '💻',
		'blend_mode': BlendModes.Screen,
		'alpha': 0.5,
		'demo': '',
		'free': True
	},
	'particles': {
		'label': '💭',
		'blend_mode': BlendModes.Screen,
		'alpha': 0.62,
		'demo': '',
		'free': True
	},
	'swamp': {
		'label': '🐸',
		'blend_mode': BlendModes.Screen,
		'alpha': 0.42,
		'demo': '',
		'free': True
	},
	'golden_dust': {
		'label': '🌞',
		'blend_mode': BlendModes.Screen,
		'alpha': 0,
		'demo': '',
		'free': True
	},
	'triangular': {
		'label': '🔺',
		'blend_mode': BlendModes.Screen,
		'alpha': -0.8,
		'demo': '',
		'free': True
	},
	'hitech_lines': {
		'label': '👩‍💻',
		'blend_mode': BlendModes.Screen,
		'alpha': 0,
		'demo': '',
		'free': True
	},
	'hitech_lines_inverted': {
		'label': '👩🏾‍💻',
		'blend_mode': BlendModes.Multiply,
		'alpha': 0,
		'demo': '',
		'free': True
	},
	'snow': {
		'label': '☃️',
		'blend_mode': BlendModes.Screen,
		'alpha': 0,
		'demo': '',
		'free': True
	}
}

class Strings:
	class Buttons:
		def Languages(lang):
			ru = "🇷🇺 Русский"
			en = "🇺🇸 English"
			return locals()[lang]
		def Vip(lang):
			ru = "Разблокировать эффекты"
			en = "Unlock effects"
			return locals()[lang]
	class Messages:
		def Language(lang):
			ru = "Выберите язык"
			en = "Choose the language"
			return locals()[lang]
		def Welcome(lang):
			ru = "Этот бот поможет анимировать ваш аватар. Выберите эффект из списка ниже.\n\nПришлите боту фотографию или воспользуйтесь командой /animate."
			en = "This bot will help you animate profile picture. Select an effect from the list below.\n\nSend a photo to the bot or use /animate command."
			return locals()[lang]
		def Wait(lang, count):
			if count > 0:
				ru = f"Ваше фото обрабатывается. Позиция в очереди: {count}"
				en = f"Your photo is being processed. Position in the queue: {count}"
			else:
				ru = "Ваше фото обрабатывается."
				en = "Your photo is being processed."
			return locals()[lang]
		def AlreadyProcessing(lang, count):
			if count - 1 > 0:
				ru = f"Ваше фото уже находится в обработке, пожалуйста подождите. Позиция в очереди: {count}"
				en = f"Your photo is already being processed, please wait. Position in the queue: {count}"
			else:
				ru = "Ваше фото уже находится в обработке, пожалуйста подождите."
				en = "Your photo is already being processed, please wait."
			return locals()[lang]
		def ErrorNoProfilePicture(lang):
			ru = f"У вас отсутствует изображение профиля. Установите его или пришлите фотографию прямо сюда."
			en = f"You do not have a profile picture. Upload it or send a photo to this chat."
			return locals()[lang]
		def ErrorProcessing(lang):
			ru = f"Произошла ошибка при обработке фото."
			en = f"An error occurred while processing the photo."
			return locals()[lang]

def Time():
	ts = int(time.time())
	return datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')