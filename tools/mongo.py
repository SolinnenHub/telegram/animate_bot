import resources as R

async def updateObject(mutex_db, collection, item):
	async with mutex_db:
		if collection.count_documents({'_id': item['_id']}) == 0:
			return collection.insert_one(item)
		return collection.replace_one({'_id': item['_id']}, item)

async def processChat(mutex_db, chats, chatId):
	async with mutex_db:
		chat = chats.find_one({'_id': chatId})
		if chat == None:
			chat = {
				'_id': chatId,
				'lang': R.Langs.EN,
				'effect': next(iter(R.Effects)),
				'free': True,
				'newbie': True
			}
			chats.insert_one(chat)
		elif not R.Effects.get(chat['effect'], None):
			chat['effect'] = next(iter(R.Effects))
			chats.replace_one({'_id': chat['_id']}, chat)
	return chat