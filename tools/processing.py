import os
from moviepy.editor import ImageClip, VideoFileClip, CompositeVideoClip
from PIL import Image
import numpy as np
import resources as R

def process(image_path, image_uuid, effect_name, duration):
	image = np.asarray(Image.open(image_path))
	if len(image.shape) == 2:
		image = np.stack((image,)*3, axis=-1)
		background = ImageClip(image)
	else:
		background = ImageClip(image_path)

	effect = VideoFileClip(f'effects/{effect_name}.mp4')

	if duration < effect.duration:
		effect = effect.subclip(0, duration)
		background = background.set_duration(duration)
	else:
		background = background.set_duration(effect.duration)

	effect = effect.resize(width=background.w)
	effect = effect.resize(height=background.h)
	if background.w > effect.w:
		effect = effect.resize(width=background.w)
	if background.h > effect.h:
		effect = effect.resize(height=background.h)

	video = CompositeVideoClip([background, effect])

	video = video.fx(R.Effects[effect_name]['blend_mode'], background, R.Effects[effect_name]['alpha'])

	output_path = f'cache_processor/{image_uuid}.mp4'
	bitrates = ['2200k', '1800k', '1500k', '1200k', '1000k', '900k']
	for bitrate in bitrates:
		video.write_videofile(output_path, codec='mpeg4', bitrate=bitrate, preset='placebo', audio=False, threads=3)
		size = os.path.getsize(output_path) / (1024*1024)
		if size <= 2:
			return output_path
		print(f"WARNING: size {round(size, 2)}mb is too big with bitrate {bitrate}.")
	return output_path