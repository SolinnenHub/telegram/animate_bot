class BlendModes:

	def Screen(clip, background, alpha):
		def func(img):
			img = img / 255
			bg = background.img / 255
			result = 1 - (1 - bg) * (1 - img)
			result = alpha*bg + (1 - alpha)*result
			return result*255
		return clip.fl_image(func)

	def Multiply(clip, background, alpha):
		def func(img):
			img = img / 255
			bg = background.img / 255
			result = bg*img
			result = alpha*bg + (1 - alpha)*result
			return result*255
		return clip.fl_image(func)