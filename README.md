# Animate image bot

This bot will help you animate profile picture. Select an effect from the list.

Send a photo to the bot or use `/animate` command.

![](demo/1.mp4)

![](demo/2.mp4)

![](demo/3.mp4)

![](demo/4.mp4)

## How to run:

```bash
conda env create -f environment.yml
./start.sh
```